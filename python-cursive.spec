%global _empty_manifest_terminate_build 0
Name:		python-cursive
Version:	0.2.3
Release:	1
Summary:	Cursive implements OpenStack-specific validation of digital signatures.
License:	Apache-2.0
URL:		http://www.openstack.org/
Source0:	https://files.pythonhosted.org/packages/aa/ec/d0e802482530a0b664c910c845cada1e490bc2af568acc0c1ed55c000502/cursive-0.2.3.tar.gz
BuildArch:	noarch

Requires:	python3-pbr
Requires:	python3-cryptography
Requires:	python3-castellan

%description


%package -n python3-cursive
Summary:	Cursive implements OpenStack-specific validation of digital signatures.
Provides:	python-cursive
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
%description -n python3-cursive


%package help
Summary:	Development documents and examples for cursive
Provides:	python3-cursive-doc
%description help


%prep
%autosetup -n cursive-0.2.3

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-cursive -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Mar 27 2023 Li Long <lilong@kylinos.cn> - 0.2.3-1
- Upgrade to 0.2.3

* Wed Jul 13 2022 Chenyx <chenyixiong3@huawei.com> - 0.2.2-3
- License compliance rectification

* Mon Jan 25 2021 zhangy1317 <zhangy1317@foxmail.com>
- Add BuildRequires python3-pip and python3-pbr
* Mon Nov 23 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
